#include "SSD1306_minimal.h"
#include <TinyWireM.h>
#include "TinyRTClib.h"
#include <avr/pgmspace.h>

SSD1306_Mini oled;
RTC_DS1307 RTC;

void PrintData() {
  oled.startScreen();
  oled.clear(); // Clears the display

  DateTime now = RTC.now();

  int day = now.day();
  int month = now.month();
  int year = now.year();
  int hour = now.hour();
  int minute = now.minute();
  int second = now.second();

  char dstr[12]; // day
  char mostr[12]; // month
  char ystr[12]; // year
  char hstr[12]; // hour
  char mstr[12]; // minute
  char sstr[12]; // second

  // Convert readings to char array
  itoa(day, dstr, 10);
  itoa(month, mostr, 10);
  itoa(year, ystr, 10);
  itoa(hour, hstr, 10);
  itoa(minute, mstr, 10);
  itoa(second, sstr, 10);

  // Print header
  oled.cursorTo(50, 0);
  oled.printString("Time");

  // Print time
  oled.cursorTo(40, 20);
  oled.printString(hstr);
  oled.printString(":");
  oled.printString(mstr);
  oled.printString(":");
  oled.printString(sstr);

  // Print date
  oled.cursorTo(32, 50);
  oled.printString(dstr);
  oled.printString("/");
  oled.printString(mostr);
  oled.printString("/");
  oled.printString(ystr);
  delay(1000);
}

void setup() {
  oled.init(0x3c); //oled address .
  oled.clear();
  TinyWireM.begin();
  RTC.begin();
  delay(200);
  if (! RTC.isrunning()) {
    Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    RTC.adjust(DateTime(__DATE__, __TIME__));
  }
}

void loop() {
  PrintData();
}
